﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendettaDev.ViewModels.Tileset
{
    public abstract class Tileset
    {
        private Bitmap image;
        private int numberPart;

        public Tileset() { }

        public Tileset(Bitmap image, int numberPart)
        {
            this.image = image;
            this.numberPart = numberPart;
        }

        protected int Height
        {
            get { return image.Height; }
        }

        protected int Width
        {
            get { return image.Width; }
        }

        protected int NumberPart
        {
            get { return numberPart; }
        }

        public int TileWidth
        {
                get { return this.getWidthPart(); }
        }

        public int TileHeight
        {
            get { return this.getHeightPart(); }
        }

        protected abstract int getHorizontalPosition(int i);
        protected abstract int getVerticalPosition(int i);
        protected abstract int getHeightPart();
        protected abstract int getWidthPart();

        private Bitmap imagePart(int x, int y, int width, int height)
        {
            System.Drawing.Rectangle rect = new System.Drawing.Rectangle(x, y, width, height);
            Bitmap imagePart = new Bitmap(width, height);

            using (Graphics g = Graphics.FromImage(imagePart))
            {
                g.DrawImage(image, 
                            new System.Drawing.Rectangle(0,0,width,height),
                            rect,
                            GraphicsUnit.Pixel);
            }
            return imagePart;
        }

        public Bitmap get(int i)
        {
            return imagePart(getHorizontalPosition(i), 
                            getVerticalPosition(i),
                            getWidthPart(),
                            getHeightPart());
        }


        public Bitmap this[int i]{
            get { return get(i);}
        }

    }
}
