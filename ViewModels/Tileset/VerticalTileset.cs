﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace VendettaDev.ViewModels.Tileset
{
    public class VerticalTileset : Tileset
    {

        public VerticalTileset(Bitmap imgPath, int nPart) : base(imgPath, nPart){}

        protected override int getHeightPart()
        {
            return Height / NumberPart;
        }

        protected override int getHorizontalPosition(int i)
        {
            return 0;
        }

        protected override int getVerticalPosition(int i)
        {
            return getHeightPart() * i;
        }

        protected override int getWidthPart()
        {
            return Width;
        }
    }
}
