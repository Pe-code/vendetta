﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendettaDev.Models.Objects.Items
{
    public interface IEquippable
    {
        bool Equip(Charactere.EvolvedRace c);
        bool Unequip(Charactere.EvolvedRace c);

    }
}
