﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VendettaDev.Models.Charactere;

namespace VendettaDev.Models.Objects.Items
{
    public abstract class Weapon : IEquippable
    {
        public abstract bool Equip(EvolvedRace c);
        public abstract bool Unequip(EvolvedRace c);
    }
}
