﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendettaDev.Models.Objects.Items
{
    public interface IConsommable
    {
        void Consume(Charactere.EvolvedRace c);
    }
}
