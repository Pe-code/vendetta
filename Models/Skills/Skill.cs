﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VendettaDev.Models.Enumerations;
using VendettaDev.Models.Others;

namespace VendettaDev.Models
{
    public class Skill : ILevellable
    {
        private EnumSkill skill;
        private int level;
        private int exp;

        private int TotalExp
        {
            get{ return level * 100; }
        }

        public float Percent
        {
            get { return (float)((exp / TotalExp) * 100.0); }
        }

        public int Level
        {
            get { return level; }
        }

        public EnumSkill Skil
        {
            get { return skill; }
        }

        public void LevelUp()
        {
            level++;
            exp = 0;
        }

        public void Progress(float amount)
        {
            exp += (int)amount;
            int r;
            if((r = exp >= TotalExp ? exp - TotalExp : -1) != -1){
                LevelUp();
                exp = r;
            }
        }
    }
}
