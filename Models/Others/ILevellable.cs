﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendettaDev.Models.Others
{
    public interface ILevellable
    {
        void LevelUp();
        void Progress(float amount);
    }
}
