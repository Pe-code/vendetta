﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendettaDev.Models.Effets
{
    public interface IAffectable
    {
        void Act(Charactere.Charactere c);
    }
}
