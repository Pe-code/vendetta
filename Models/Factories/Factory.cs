﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendettaDev.Models.Factories
{
    public abstract class Factory<T,E>
    {
        public abstract T Get(E element);

        public T this[E element]
        {
            get { return Get(element); }
        }

    }
}
